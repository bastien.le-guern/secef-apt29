# SECEF PupyRAT

Original PupyRAT : https://github.com/n1nj4sec/pupy  
This version has been slightly modified to be used on SECEF Infrastructure to reproduce APT29 Emulation Plan Day 1 : https://github.com/mitre-attack/attack-arsenal/tree/master/adversary_emulation/APT29/Emulation_Plan/Day%201 

## WARNING : PupyRAT is outdated and might only work on old Linux version! It is not guaranteed that the version on this repository will work on another machine that the one on which it was tested!

## Installation & Usage

Install these packages :
```sh
sudo apt-get install git libssl1.0-dev libffi-dev python-dev \
	python-pip build-essential swig tcpdump
```

Clone the repository:
```sh
git clone https://gitlab-student.centralesupelec.fr/bastien.le-guern/secef-pupyrat.git
```

Launch PupyRAT :
```sh
cd secef secef-pupyrat/secef-pupy/
./pupyws/bin/pupysh
```

Enjoy!
